﻿using npc_sync_service.Api.Models;

namespace npc_sync_service.Api.Response
{
    public class ProductInfo
    {
        public Guid Id { get; set; }
        public string genericName { get; set; }
        public string SN { get; set; }
        public string npcId { get; set; }
        public string status { get; set; }
    }
}
