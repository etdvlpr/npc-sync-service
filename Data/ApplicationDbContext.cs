﻿using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Duende.IdentityServer.EntityFramework.Options;
using npc_sync_service.Api.Models;
using npc_sync_service.Models;

namespace npc_sync_service.Data;

public class ApplicationDbContext : ApiAuthorizationDbContext<ApplicationUser>
{

    public readonly DbContextOptions _options;
    public readonly IOptions<OperationalStoreOptions> _operationalStoreOptions;

    public ApplicationDbContext(DbContextOptions options, IOptions<OperationalStoreOptions> operationalStoreOptions) : base(options, operationalStoreOptions)
    {
        _options = options;
        _operationalStoreOptions = operationalStoreOptions;
    }
    
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        var connectionString = "server=localhost; port=3306; database=cqrs; user=root; password=P4$$w0rd;";
        optionsBuilder.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString));
    }
    public DbSet<Product> Products { get; set; }
    public DbSet<Manufacturer> Manufacturers { get; set; }
    public DbSet<Country> Countries { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        modelBuilder.Entity<Product>().ToTable("product");
        modelBuilder.Entity<Manufacturer>().ToTable("manufacturer");
        modelBuilder.Entity<Country>().ToTable("country");
        //configure primary keys
        modelBuilder.Entity<Product>().HasKey(p => p.Id);
        modelBuilder.Entity<Manufacturer>().HasKey(m => m.Id);
        modelBuilder.Entity<Country>().HasKey(c => c.Id);

        //configure indexes
        modelBuilder.Entity<Product>().HasIndex(p => p.SN).IsUnique();
        modelBuilder.Entity<Manufacturer>().HasIndex(m => m.Code).IsUnique();
        modelBuilder.Entity<Country>().HasIndex(c => c.Code).IsUnique();

        // create foreign keys
        // one-to-many with Product and Manufacturer using manufacturerCode
        modelBuilder.Entity<Product>()
            .HasOne<Manufacturer>(p => p.Manufacturer)
            .WithMany(m => m.Products)
            .HasPrincipalKey(m => m.Code);
        // one-to-many relation with country and manufacturer using countryCode and country.code
        modelBuilder.Entity<Manufacturer>()
            .HasOne<Country>(m => m.Country)
            .WithMany(c => c.Manufacturers)
            .HasPrincipalKey(c => c.Code);
    }
}
