﻿using Microsoft.EntityFrameworkCore;
using npc_sync_service.Api.Response;
using npc_sync_service.Data;

namespace npc_sync_service.Api.Queries.Handlers
{
    public class ProductQueryHandler
    {
        private readonly ApplicationDbContext _dbContext;


        public ProductQueryHandler(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<Models.Product> Handle(GetAllProductInfoQuery _)
        {
            _dbContext.Database.EnsureCreated();
            var products = _dbContext.Products.Include(p => p.Manufacturer).ToList();
            return products;
        }

        public ProductInfo Handle(GetProductInfoQuery query)
        {
            var product = _dbContext.Set<Models.Product>()
                .FirstOrDefault(p => p.SN == query.SN);

            if (product == null)
            {
                throw new Exception($"Cannot find product '{query.SN}'.");
            }

            return new ProductInfo
            {
                Id = product.Id,
                genericName = product.genericName,
                status = product.status,
                SN = product.SN,
                npcId = product.npcId,
            };
        }
    }
}
