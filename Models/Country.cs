using System.Text.Json.Serialization;

namespace npc_sync_service.Api.Models
{
    public class Country
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        [JsonIgnore]
        public ICollection<Manufacturer>? Manufacturers { get; set; }
    }   
}