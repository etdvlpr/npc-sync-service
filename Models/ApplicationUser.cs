﻿using Microsoft.AspNetCore.Identity;

namespace npc_sync_service.Models;

public class ApplicationUser : IdentityUser
{
}
