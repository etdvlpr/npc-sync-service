using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace npc_sync_service.Api.Models
{
    public class Manufacturer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        [ForeignKey("CountryCode")]
        public Country? Country { get; set; }
        public string? Description { get; set; }
        public string? gln { get; set; }
        [JsonIgnore]
        public ICollection<Product> Products { get; set; }

    }
}