﻿using System.ComponentModel.DataAnnotations.Schema;

namespace npc_sync_service.Api.Models
{
    public class Product
    {
        public Guid Id { get; set; }
        public string? npcId { get; set; }
        public string? SN { get; set; }
        public string genericName { get; set; }
        public string? productDescription { get; set; }
        public string dosageForm { get; set; }
        public string dosageStrength { get; set; }
        public string dosageUnit { get; set; }
        public string brandName { get; set; }
        public string? hierarchy { get; set; }
        public string? GTIN { get; set; }
        public string status { get; set; } = "Added to database";

        //Navigation properties
        [ForeignKey("ManufacturerCode")]
        public Manufacturer? Manufacturer { get; set; }
    }
}
