import React, { Component } from 'react';

export class Home extends Component {
  static displayName = Home.name;

  render () {
    return (
      <div>
        <h1>NPC sync service</h1>
        <p>This service is built to run synchronizations between the NPC currently (<a href='https://npc.sandboxaddis.com/'>https://npc.sandboxaddis.com/</a>) with other services</p>
        <p>Steps</p>
        <ul>
          <li>External service sends product data to this service</li>
          <li>The service then sends this product data to OpenFn</li>
          <li>Once openFn has created the product entity on the NPC it will send the newly created product ID to this service</li>
          <li>Finally this service maintains track of products both in the npc and externally along with their status</li>
        </ul>
      </div>
    );
  }
}
