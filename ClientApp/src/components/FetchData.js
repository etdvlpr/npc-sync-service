import React, { Component } from 'react';
import authService from './api-authorization/AuthorizeService'

export class FetchData extends Component {
  static displayName = FetchData.name;

  constructor(props) {
    super(props);
    this.state = { products: [], loading: true };
  }

  componentDidMount() {
    this.populateProductData();
  }

  static renderProductsTable(products) {
    return (
      <table className='table table-striped' aria-labelledby="tabelLabel">
        <thead>
          <tr>
            <th>Genereic Name</th>
            <th>SN</th>
            <th>NPC ID</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {products.map(product =>
            <tr key={product.Id}>
              <td>{product.genericName}</td>
              <td>{product.SN}</td>
              <td>{product.npcId}</td>
              <td>{product.status}</td>
            </tr>
          )}
        </tbody>
      </table>
    );
  }

  render() {
    let contents = this.state.loading
      ? <p><em>Loading...</em></p>
      : FetchData.renderProductsTable(this.state.products);

    return (
      <div>
        <h1 id="tabelLabel" >Product Data</h1>
        <p>This is a list of products registered on this service.</p>
        {contents}
      </div>
    );
  }

  async populateProductData() {
    const token = await authService.getAccessToken();
    const response = await fetch('api/product', {
      headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    const data = await response.json();
    this.setState({ products: data, loading: false });
  }
}
