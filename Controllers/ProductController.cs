﻿using Microsoft.AspNetCore.Mvc;
using npc_sync_service.Api.Commands.Handlers;
using npc_sync_service.Api.Queries;
using npc_sync_service.Api.Queries.Handlers;
using npc_sync_service.Api.Response;
using npc_sync_service.Api.Request;
using Microsoft.AspNetCore.Authorization;

namespace npc_sync_service.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly ProductCommandHandler _commandHandler;
        private readonly ProductQueryHandler _queryHandler;

        public ProductController(
            ProductCommandHandler commandHandler,
            ProductQueryHandler queryHandler
        )
        {
            _commandHandler = commandHandler;
            _queryHandler = queryHandler;
        }
        [Authorize]
        [HttpGet]
        public IEnumerable<Models.Product> GetAllProductInfos()
        {
            var query = new GetAllProductInfoQuery();
            return _queryHandler.Handle(query);
        }
        [Authorize]
        [HttpGet("{sn}")]
        public ProductInfo GetProductInfo(string sn)
        {
            var query = new GetProductInfoQuery { SN = sn };
            return _queryHandler.Handle(query);
        }

        [HttpPost]
        public void CreateProduct([FromBody] CreateProductRequest request)
        {
            _commandHandler.Handle(request);
        }

        [HttpPost("set_npcid")]
        public void UpdateProduct([FromBody] UpdateProductRequest request)
        {
            _commandHandler.Handle(request);
        }

    }
}
