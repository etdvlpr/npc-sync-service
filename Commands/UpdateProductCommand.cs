using npc_sync_service.Api.Models;

namespace npc_sync_service.Api.Commands{
    public class UpdateProductCommand{
        public string SN { get; set; }
        public string npcId { get; set; }
    }
}