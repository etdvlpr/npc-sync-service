﻿using System.Text.RegularExpressions;
using Duende.IdentityServer.EntityFramework.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using npc_sync_service.Api.Models;
using npc_sync_service.Api.Request;
using npc_sync_service.Data;

namespace npc_sync_service.Api.Commands.Handlers
{
    public class ProductCommandHandler
    {
        private readonly IConfiguration configuration;
        private readonly ApplicationDbContext _dbContext;
        private static readonly HttpClient client = new HttpClient();

        public ProductCommandHandler(ApplicationDbContext dbContext, IConfiguration configuration)
        {
            _dbContext = dbContext;
            this.configuration = configuration;
        }

        public void Handle(UpdateProductRequest request)
        {
            var product = _dbContext.Set<Models.Product>()
                   .FirstOrDefault(p => p.SN == request.SN);

            if (product == null)
            {
                throw new Exception($"Cannot find product '{request.SN}'.");
            }
            product.npcId = request.npcId;
            _dbContext.SaveChanges();
        }

        public void Handle(CreateProductRequest request)
        {
            _dbContext.Database.EnsureCreated();
            Models.Product product = getProductFromRequest(request);

            _dbContext.Products.Add(product);
            _dbContext.SaveChanges();

            sendToOpenFnAsync(product, _dbContext._options, _dbContext._operationalStoreOptions);
        }


        private async Task sendToOpenFnAsync(Models.Product product, DbContextOptions options, IOptions<OperationalStoreOptions> operationalStoreOptions)
        {
            //set api key
            var apiKey = configuration.GetValue<string>("OpenFnApiKey");
            var url = configuration.GetValue<string>("OpenFnUrl");

            client.DefaultRequestHeaders.Add("x-api-key", apiKey);
            var response = await client.PostAsJsonAsync(url, product);

            using (var context = new ApplicationDbContext(options, operationalStoreOptions))
            {
                //update product status
                var productToUpdate = context.Products.FirstOrDefault(p => p.Id == product.Id);
                if (productToUpdate != null)
                {
                    productToUpdate.status = response.IsSuccessStatusCode ? "Sent to OpenFn" : "Failed to send to OpenFn";
                    context.SaveChanges();
                }
            }
        }

        private Models.Product getProductFromRequest(CreateProductRequest request)
        {
            Models.Manufacturer? manufacturer = null;
            Models.Country? country = null;
            if (request.Manufacturer?.Country != null)
            {
                //check if country already exists
                if (_dbContext.Countries.Any())
                {
                    country = _dbContext.Set<Models.Country>().FirstOrDefault(c => c.Code == request.Manufacturer.Country.Code);
                }
                if (country == null)
                {
                    //add country
                    country = new Models.Country
                    {
                        Code = request.Manufacturer.Country.Code,
                        Name = request.Manufacturer.Country.Name,
                    };
                    _dbContext.Countries.Add(country);
                    _dbContext.SaveChanges();
                }
            }
            if (request.Manufacturer != null)
            {
                //regex replace all non-alphanumeric characters with space from name and assign to code
                string manufacturerCode = Regex.Replace(request.Manufacturer.Name.Trim().ToLower(), @"[^a-zA-Z0-9]", "_") + "_" + request.Manufacturer.Country.Code;

                if (_dbContext.Manufacturers.Any())
                {
                    //check if manufacturer already exists
                    manufacturer = _dbContext.Set<Models.Manufacturer>().FirstOrDefault(m => m.Code == manufacturerCode);
                }
                if (manufacturer == null)
                {
                    manufacturer = new Models.Manufacturer
                    {
                        Name = request.Manufacturer.Name,
                        Code = manufacturerCode,
                        Country = country,
                        Description = request.Manufacturer.description,
                    };
                    _dbContext.Manufacturers.Add(manufacturer);
                    _dbContext.SaveChanges();
                }
            }
            Models.Product product = new Models.Product();
            product.genericName = request.genericName;
            product.productDescription = request.productDescription;
            product.dosageForm = request.dosageForm;
            product.dosageStrength = request.dosageStrength;
            product.dosageUnit = request.dosageUnit;
            product.brandName = request.brandName;
            product.hierarchy = request.hierarchy;
            product.GTIN = request.GTIN;
            product.SN = request.SN;
            product.Manufacturer = manufacturer;
            product.status = "Created";
            return product;
        }
    }
}
