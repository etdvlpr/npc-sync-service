using npc_sync_service.Api.Models;

namespace npc_sync_service.Api.Commands{
    public class CreateProductCommand{
        public Guid Id { get; set; }
        public string genericName { get; set; }
        public string? productDescription { get; set; }
        public string dosageForm { get; set; }
        public string dosageStrength { get; set; }
        public string dosageUnit { get; set; }
        public string brandName { get; set; }
        public string? hierarchy { get; set; }
        public string? GTIN { get; set; }
        public string? SN { get; set; }

        public Manufacturer? Manufacturer { get; set; }        
    }
}