namespace npc_sync_service.Api.Request
{
    public class CreateProductRequest
    {
        public string genericName { get; set; }
        public string? SN { get; set; }
        public string? productDescription { get; set; }
        public string dosageForm { get; set; }
        public string dosageStrength { get; set; }
        public string dosageUnit { get; set; }
        public string brandName { get; set; }
        public string? hierarchy { get; set; }
        public string? GTIN { get; set; }
        public Manufacturer? Manufacturer { get; set; }
    }
    public class Manufacturer
    {
        public string Name { get; set; }
        public string? description { get; set; }
        public Country? Country { get; set; }
    }

    public class Country
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }
}