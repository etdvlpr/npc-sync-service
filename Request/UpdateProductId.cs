namespace npc_sync_service.Api.Request
{
    public class UpdateProductRequest
    {
        public string SN { get; set; }
        public string npcId { get; set; }
    }
}